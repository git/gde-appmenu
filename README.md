# gde_appmenu

an application menu for gtk+ based desktop environments

gde_appmenu is an implementation of the application menu common to these
found in GNOME 2.0, Mate, XFCE and LXDE on GNU/Linux and Unix OSes. This
is however independent of any particular desktop environment and can
provide a quick and simple access to the installed desktop applications
in any context where the desktop provides no direct access. It works in
X11 and Wayland sessions.

gde_appmenu provides a simple floating, movable, window initially at the
top left corner of the screen, with an application menu that drops down
to offer various application categoreis as defined by the desktop
session active at the time. The categories follow the Freedesktop.org
menu spec standard.

## setup and build
gde_appmenu is written in GNU Guile and requires

gtk+ 3.0
glib 2.0
GObject-inspection 0.72 (or above)
GNU guile 3.0
guile-lib
GNU G-Golf (https://www.gnu.org/software/g-golf/)

installed on your system.

From sources, gde_appmenu can be built via the usual GNU convention,

that is,

./configure --prefix=<install dir>
make
make install

## run

To run it from the command line, first make sure
G-Golf's pkgconfig install directory is in the pgk-config
path (PKG_CONFIG_PATH), by, for example,

export PKG_CONFIG_PATH=/share/software/g_golf/installed/lib/pkgconfig/

and then

It can be run from the commmand line via

(install dir)/bin/gde_appmenu.scm

or just

gde_appmenu.scm

if the file is in your $PATH.

On GNU Guix, With the supplied guix.scm, you can obtain a shell with dependencies
set up already via

guix shell -D -f guix.scm

and then you can run directly from the source tree via

scripts/gde_appmenu.scm

This version only shows the English name of the applications; locale
support (showing translated application names per the locale) is a task
in the roadmap.

## license

Copyright 2022 Li-Cheng (Andy) Tai
atai@atai.org

This program is licensed under the GNU General Public License, version 3
or later, as published by the Free Software Foundation. See the license
text in the file COPYING.

gde_appmenu is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

Hopefully this program is useful to you.

## changes
   Version 0.1.2    20221123
      * properly search for .desktop files via XDG_DATA_DIRS
      * a guix.scm for development setup under GNU Guix

   Version 0.1      20221002
      * initial release

## bugs

To report bugs, go to https://savannah.nongnu.org/bugs/?group=gde-appmenu&func=additem

## code web site and support

gde_appmenu has a main repo page at https://savannah.nongnu.org/projects/gde-appmenu/
The sources can be accessed vi git, and a mailing list for gde_appmenu is at gde-appmenu-user@nongnu.org
Please visit the main repo page for details.

## roadmap

* locale support
* Wayland support improvement
* port to gtk 4
