#! /bin/sh
# -*- mode: scheme; coding: utf-8 -*-
export GUILE_LOAD_PATH=`pkg-config g-golf-1.0 --variable=sitedir`:`pwd`:${GUILE_LOAD_PATH}
export GUILE_LOAD_COMPILED_PATH=`pkg-config g-golf-1.0 --variable=siteccachedir`:`pwd`:${GUILE_LOAD_COMPILED_PATH}
export LD_LIBRARY_PATH=`pkg-config g-golf-1.0 --variable=libdir`:${LD_LIBRARY_PATH}
local_dir=$(dirname $0)
local_lib_dir=${local_dir}/../share/guile/site/3.0  #follow make install directory hierarchy of autotools
export GUILE_LOAD_PATH=${local_dir}:${local_lib_dir}:${GUILE_LOAD_PATH}
export LD_LIBRARY_PATH=`pkg-config g-golf-1.0 --variable=libdir`:${LD_LIBRARY_PATH}

exec guile -e main -s "$0" "$@"
#exec valgrind guile -e main -s "$0" "$@"
!#

;;;     Copyright 2022 Li-Cheng (Andy) Tai
;;;                      atai@atai.org
;;;
;;;     gde_appmenu is free software: you can redistribute it and/or modify it
;;;     under the terms of the GNU General Public License as published by the Free
;;;     Software Foundation, either version 3 of the License, or (at your option)
;;;     any later version.
;;;
;;;     gde_appmenu is distributed in the hope that it will be useful, but WITHOUT
;;;     ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;;;     FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;;;     more details.
;;;
;;;     You should have received a copy of the GNU General Public License along with
;;;     gde_appmenu. If not, see http://www.gnu.org/licenses/.


(use-modules (ice-9 format)
             (ice-9 hash-table)
             (ice-9 match)
             (ice-9 popen)
             (ice-9 rdelim)
             (ice-9 receive)
             (logging logger) ; from guile-lib
             (oop goops)
             (os process)  ; from guile-lib
             ((rnrs) :version (6) #:prefix r6rs:)
             (srfi srfi-1)
             (system vm trace)
             (sxml simple)
             (sxml match)
             (scripts logging)
             (scripts utils)
             )

(setlocale LC_ALL "")
                                        ; constant strings
(define str_home "HOME")
(define str_applications "Applications")
(define str_xdg_data_dirs "XDG_DATA_DIRS")
(define str_xdg_config_dirs "XDG_CONFIG_DIRS")
(define str_xdg_menu_prefix "XDG_MENU_PREFIX")
(define str_application "Application")
(define str_desktop_entry "[Desktop Entry]")
(define str_menu_file_suffix "applications.menu")
(define str_applications_dir "/applications")

;; .desktop file fields
(define str_name "Name")
(define str_categories "Categories")
(define str_exec "Exec")
(define str_terminal "Terminal")
(define str_type "Type")
(define str_other "Other")

(define init_pos_x 10)  ; initial windows position
(define init_pos_y 10)
                                        ; end constant strings

                                        ; classes

(define-class <menu> ()
  (name #:init-value "" #:init-keyword #:name #:accessor name)
  (children #:init-value '() #:accessor children)
  (parent #:init-value #f #:accessor parent)
  (+category #:init-value '() #:accessor +category)
  (-category #:init-value '() #:accessor -category)
  (gtk_mwnu_item #:init-value #f #:accessor gtk-menu-item)
  (gtk_mwnu #:init-value #f #:accessor gtk-menu)
  )

(define-method (menu-add-child (parent_menu <menu>) (child_menu <menu>))
  (set! (parent child_menu) parent_menu)
  (set! (children parent_menu) (append (children parent_menu) (list child_menu)))
  (log-msg 'DEBUG (format #f "~:a parent of ~:a ~%" (name parent_menu) (name child_menu)))
  )

(define menu_list '())

(define* (make-menu name_string  #:optional parent)
  (let ((menu (make <menu> #:name name_string)))
    (when (not (eq? parent #f))
      (menu-add-child parent menu))
    (set! menu_list (append menu_list (list menu)))  ; add to global menu list
    menu))

(define top_menu (make-menu str_applications))
(define menu_stack (list top_menu))
(define-method (menu_stack_push (menu_item <menu>))
  (set! menu_stack (append (list menu_item) menu_stack)))

(define (menu_stack_top)
  (car menu_stack))

(define (menu_stack_pop)
  (match menu_stack
         ((head . rest)
          (set! menu_stack rest))))

(define-class <application> ()
  (name #:init-value "" #:init-keyword #:name #:accessor name)
  (exec #:init-value "" #:accessor exec)
  (type #:init-value "" #:accessor type)
  (categories #:init-value '() #:accessor categories)
  (terminal #:init-value "" #:accessor terminal)
  (gtk_mwnu_item #:init-value #f #:accessor gtk-menu-item)
  )

(define application_name_table (make-hash-table)) ; hash table of name string to application
                                        ; end classes

(define (find-files-in-dirs dirs pattern) ; dirs is list of directories, pattern is a regex
  (append-map (lambda (dir) (find-files dir pattern)) dirs))

(define (find-desktop-files)
  (let ((data_dirs  (search-path-as-string->list (getenv str_xdg_data_dirs))))
  	  ;;search directories in XDG_DATA_DIRS each appended by "/applications"
  	(set!  data_dirs  (map  (lambda (dir) (string-append dir str_applications_dir)) data_dirs))
    (if (null? data_dirs)
        (#f)
        (find-files-in-dirs data_dirs ".*\\.desktop$")
        )))

(define (find-menu-files)
  (let ((config_dirs  (search-path-as-string->list (getenv str_xdg_config_dirs) ))
        (result '())
        (prefix (getenv str_xdg_menu_prefix)))
    (if (null? config_dirs)
        result
        (begin
          (when (not (equal? #f prefix))
            (set! result
                  (find-files-in-dirs config_dirs
                                      (string-append  prefix  str_menu_file_suffix))))
          (if (null? result)
              (find-files-in-dirs config_dirs str_menu_file_suffix)
              result)
          ))))

(define top_menu_bar_gtk #f)
(define top_menu_gtk #f)
(define top_menu_item_gtk #f)  ; top level gtk objects whose life times are global

(eval-when (expand load eval)
           (use-modules (oop goops))

           (default-duplicate-binding-handler
             '(merge-generics replace warn-override-core warn last))

           (use-modules (g-golf))
           (use-modules (g-golf glib))
           (g-irepository-require "GLib")
           (for-each (lambda (name)
                       (gi-import-by-name "GLib" name))
                     '("markup_escape_text"
                       "Source"))
           (g-irepository-require "Gdk" #:version "3.0")
           (for-each (lambda (name)
                       (gi-import-by-name "Gdk" name))
                     '("Display"
                       "EventOwnerChange"
                       "Atom"))
           (g-irepository-require "Gtk" #:version "3.0")
           (for-each (lambda (name)
                       (gi-import-by-name "Gtk" name))
                     '("Application"
                       "ApplicationWindow"
                       "Menu"
                       "MenuBar"
                       "MenuItem"
                       "MenuShell"
                       "main_quit"
                       ))

           (g-irepository-require "Pango")
           (for-each (lambda (name)
                       (gi-import-by-name "Pango" name))
                     '("EllipsizeMode"))
           )


(define (read-as-sxml file_path)
  (let ((port (open-input-file file_path))
        (content  ""))

    (with-exception-handler (lambda (ex)
                              (log-msg 'DEBUG  (format #f "exception when reading ~:a : ~:a ~%" file_path ex)))
                            (set! content (xml->sxml port #:trim-whitespace? #t))
                            #:unwind? #t)
    (close-input-port port)
    content))

(define (activate-callback app)
  (let* ((menu_files (find-menu-files))
         (desktop_files (find-desktop-files))
         (menu_file (car menu_files))
         (menu_sxml (read-as-sxml menu_file))

         (window (make <gtk-window>
                   #:application app
                   #:default-height 22 ; TODO: adjust per current font size
                   #:default-width 200  ; TODO: adjust per current font siz
                   #:title "Application Menu")))

    (define (parse-menu sxml_tree)
      (log-msg 'DEBUG (format #f "parse_menu on '~:a' ~%" sxml_tree))
      (when (not (null? sxml_tree))
        (sxml-match sxml_tree
                    ((*TOP* ,menu_item)
                     (parse-menu menu_item))
                    ((Menu  ,menu_item . ,rest)
                     (parse-menu menu_item)
                     (for-each parse-menu rest)
                     (menu_stack_pop))
                    ((Name . ,rest)
                     (let ((name (string-join rest)))
                       (log-msg 'DEBUG (format #f "Menu: ~:a ~%" name))
                       (when (not (equal? name str_applications))
                         (let ((new_menu (make-menu name  (menu_stack_top))))
                           (when (not (eq? (parent new_menu) #f))
                             (let ((gtk_menu_item (make <gtk-menu-item> #:label name)) ; note not by default  #:visible #t we only make menu visible if it has children
                                   (gtk_menu (make <gtk-menu> #:visible #t)))
                               (set! (gtk-menu-item new_menu) gtk_menu_item)
                               (set! (gtk-menu new_menu) gtk_menu)
                               (gtk-menu-item-set-submenu gtk_menu_item gtk_menu)
                               (gtk-menu-shell-append (gtk-menu (parent new_menu)) gtk_menu_item)
                               ))

                           (menu_stack_push new_menu)
                           ))))
                    ((Include ,category  . ,rest)
                     (log-msg 'DEBUG (format #f "Include ~:a ~%" rest))
                     (parse-menu category)
                     (for-each parse-menu rest))
                    ((And  ,category  . ,rest)
                     (log-msg 'DEBUG (format #f "And ~:a ~:a ~%" category rest))
                     (parse-menu category)
                     (for-each parse-menu rest))
                    ((Not (Category ,category))
                     (log-msg 'DEBUG (format #f "Not Category ~:a ~%" category))
                     (set! (-category (menu_stack_top)) (append (-category (menu_stack_top)) (list category))))
                    ((Category ,category)
                     (log-msg 'DEBUG (format #f "Category ~:a ~%" category))
                     (set! (+category (menu_stack_top)) (append (+category (menu_stack_top)) (list category))))

                    ((Directory ,directory)
                     (log-msg 'DEBUG (format #f "Directory ~:a ~%" directory))
                     #t ) ; TODO
                    ((Filename ,filename)
                     (log-msg 'DEBUG (format #f "Filename ~:a ~%" filename))
                     #t ) ; TODO

                    (,otherwise
                     #f) ; ignore
                    )

        ))

    (define (parse-desktop-file file_path)
      (let ((value "")
            (strs "")
            (application #f))

        (define (first-matching-menu categories)
          (let search ((menus menu_list))
            (define (matching menu)
              (let ((p (lset-intersection string=? categories (+category menu)))
                    (n (lset-intersection string=? categories (-category menu))))
                (if (and (null? n) (not (null? p)))
                    #t
                    #f)))
            (define m1 (car menus))
            (define mrest (cdr menus))

            (if (matching m1)
                m1
                (if (null? mrest)
                    #f
                    (search mrest)))))

        (define (process-categories categories)
          (let ((menu (first-matching-menu categories))
                (gtk_menu_item #f))
            (when (and (equal? (gtk-menu-item application) #f)
                       (not (equal? menu #f)))
              (log-msg 'DEBUG (format #f "put app  '~:a' in menu  '~:a' ~%"  (name application) (name menu)))
              (set! (children menu) (append (children menu) (list application)))
              (set! gtk_menu_item (make <gtk-menu-item> #:label (name application) #:visible #t))
              (set! (gtk-menu-item application) gtk_menu_item)
              (gtk-menu-shell-append (gtk-menu menu) gtk_menu_item)
              (connect gtk_menu_item 'activate
                       (lambda (_)
                         (let* ((cmd_line (exec application))
                                (cmd_invoke `(run-concurrently ,@(string-split cmd_line char-set:whitespace))))
                           (log-msg 'INFO (format #f "run '~:a' ~%" cmd_line))
                           (primitive-eval cmd_invoke))))
              )))
        (set! value (read-desktop-file-value file_path str_name))
        (set! application (hash-ref application_name_table value))
        (when (equal? application #f)
          (set! application (make <application>))
          (set! (name application) value)
          (hash-set! application_name_table value application)
          (set! value (read-desktop-file-value file_path str_exec))
          (set! value (string-trim-both (substring-replace value "(%f|%F|%u|%U)" "")))
          (set! (exec application) value)
          (set! value (read-desktop-file-value file_path str_terminal))
          (set! (terminal application) value)
          (set! value (read-desktop-file-value file_path str_type))
          (set! (type application) value)
          (set! value (read-desktop-file-value file_path str_categories))
          (set! (categories application) (string-tokenize value (char-set-complement (string->char-set ";"))))
          (set! (categories application) (append (categories application) (list str_application))) ; make application appearing in 'Other' if no fitting category found

          (when (and (not (string-null? (name application)))
                     (not (string-null? (exec application)))
                     (not (equal? (terminal application) "true"))
                     (equal? (type application) str_application)
                     )
            (process-categories (categories application)))

          )
        )

      )
    (set-app-menu app #f)
    (set! top_menu_bar_gtk (make <gtk-menu-bar> #:parent window #:visible #t))
    (set! top_menu_gtk (make <gtk-menu> #:visible #t))
    (set! top_menu_item_gtk (make <gtk-menu-item> #:label (name top_menu) #:visible #t))

    (connect window
             'delete-event
             (lambda (window event)
               (gtk-widget-destroy window)
               #f)) ;; do not stop the event propagation
    (connect window 'destroy
             (lambda (object)
               (gtk-main-quit)
               #f))

    (set! (gtk-menu-item top_menu) top_menu_item_gtk)
    (set! (gtk-menu top_menu) top_menu_gtk)
    (gtk-menu-item-set-submenu top_menu_item_gtk top_menu_gtk)
    (menu_stack_push top_menu)
    (gtk-menu-shell-append top_menu_bar_gtk top_menu_item_gtk)
    (parse-menu menu_sxml)

    (for-each parse-desktop-file desktop_files)

    (for-each
     (lambda (menu)
       (when (not (null? (children menu)))
         (set-visible (gtk-menu-item menu) #t)))
     menu_list)

    (set-type-hint window 'utility)
    (move window init_pos_x init_pos_y)
    (set-decorated window #f)
    (log-msg 'DEBUG (format #f "before showing window ~%"))
    (show window)
    (set-keep-above window #t)
    (log-msg 'DEBUG (format #f "begin gtk event loop ~%"))
    ))



(define (main args)
  (let ((prog_name "gde_appmenu")
        (version_string "0.1.2")
        (app (make <gtk-application>
               #:application-id "org.atai.gde_appmenu")))
    (setup-logging)
    (format #t "~:a ~:a Copyright 2022 Li-Cheng (Andy) Tai ~%" prog_name  version_string)
    (format #t "License: GNU GPL3+ ~%")
    (connect app 'activate activate-callback)
    (let ((status (g-application-run app (length args) args)))
      (log-msg 'DEBUG (format #f "exited gtk event loop, exit status ~:d ~%" status))
      (shutdown-logging)
      (exit status))))
